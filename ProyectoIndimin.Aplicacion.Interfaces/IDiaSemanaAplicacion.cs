﻿using ProyectoIndimin.Aplicacion.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Aplicacion.Interfaces
{
    public interface IDiaSemanaAplicacion
    {
        Task<Response<DiaSemanaDTO>> Get(int valueObjectId);
        Task<Response<IEnumerable<DiaSemanaDTO>>> GetAll();
    }
}
