﻿using ProyectoIndimin.Aplicacion.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Aplicacion.Interfaces
{
    public interface ICiudadanoAplicacion
    {
        Task<Response<bool>> Insert(CiudadanoDTO valueObject);
        Task<Response<bool>> Update(CiudadanoDTO valueObject);
        Task<Response<bool>> Delete(int valueObjectId);
        Task<Response<CiudadanoDTO>> Get(int valueObjectId);
        Task<Response<IEnumerable<CiudadanoDTO>>> GetNombre(string valueObjectId);
        Task<Response<IEnumerable<CiudadanoDTO>>> GetAll();
    }
}
