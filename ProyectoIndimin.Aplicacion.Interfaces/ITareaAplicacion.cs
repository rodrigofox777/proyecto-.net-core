﻿using ProyectoIndimin.Aplicacion.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Aplicacion.Interfaces
{
    public interface ITareaAplicacion
    {
        Task<Response<bool>> Insert(TareaDTO valueObject);
        Task<Response<bool>> Update(TareaDTO valueObject);
        Task<Response<bool>> Delete(int valueObjectId);
        Task<Response<TareaDTO>> Get(int valueObjectId);
        Task<Response<IEnumerable<TareaDTO>>> GetAll();
        Task<Response<IEnumerable<TareaDTO>>> GetTareabyDay(int valueObjectId);
    }
}
