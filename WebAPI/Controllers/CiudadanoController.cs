﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProyectoIndimin.Aplicacion.DTO;
using ProyectoIndimin.Aplicacion.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CiudadanoController : Controller
    {
        private readonly ICiudadanoAplicacion _ciudadanoAplicacion;

        public CiudadanoController(ICiudadanoAplicacion ciudadanoAplicacion)
        {
            _ciudadanoAplicacion = ciudadanoAplicacion;
        }

        [HttpPost("Insert")]
        public async Task<IActionResult> Insert([FromBody] CiudadanoDTO ciudadanoDTO)
        {
            if (ciudadanoDTO == null)
                return BadRequest();
            var response = await _ciudadanoAplicacion.Insert(ciudadanoDTO);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromBody] CiudadanoDTO ciudadanoDTO)
        {
            if (ciudadanoDTO == null)
                return BadRequest();
            var response = await _ciudadanoAplicacion.Update(ciudadanoDTO);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpDelete("Delete/{ciudadanoID}")]
        public async Task<IActionResult> Delete(int ciudadanoID)
        {
            if (ciudadanoID<=0)
                return BadRequest();
            var response = await _ciudadanoAplicacion.Delete(ciudadanoID);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("Get/{ciudadanoID}")]
        public async Task<IActionResult> Get(int ciudadanoID)
        {
            if (ciudadanoID <= 0)
                return BadRequest();
            var response = await _ciudadanoAplicacion.Get(ciudadanoID);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("GetNombre/{ciudadanoNombre}")]
        public async Task<IActionResult> GetNombre(string ciudadanoNombre)
        {
            if (string.IsNullOrEmpty(ciudadanoNombre))
                return BadRequest();
            var response = await _ciudadanoAplicacion.GetNombre(ciudadanoNombre);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = await _ciudadanoAplicacion.GetAll();
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }
    }
}
