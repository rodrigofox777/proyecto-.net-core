﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dominio;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Persistencia;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly VehiculoContext _vehiculoContext;


        public WeatherForecastController(VehiculoContext vehiculoContext)
        {
            _vehiculoContext = vehiculoContext;
        }

        [HttpGet]
        public IEnumerable<Vehiculo> Get()
        {
            return _vehiculoContext.Vehiculo.ToList();
        }

        [HttpPost]
        public void CrearVehiculo(string data)
        {
           
        }
    }
}
