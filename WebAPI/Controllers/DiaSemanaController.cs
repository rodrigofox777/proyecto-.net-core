﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProyectoIndimin.Aplicacion.Interfaces;

namespace ProyectoIndimin.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiaSemanaController : Controller
    {
        private readonly IDiaSemanaAplicacion _diaSemanaAplicacion;

        public DiaSemanaController(IDiaSemanaAplicacion diaSemanaAplicacion)
        {
            _diaSemanaAplicacion = diaSemanaAplicacion;
        }

        [HttpGet("Get/{idDia}")]
        public async Task<IActionResult> Get(int idDia)
        {
            if (idDia <= 0)
                return BadRequest();
            var response = await _diaSemanaAplicacion.Get(idDia);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = await _diaSemanaAplicacion.GetAll();
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }
    }
}
