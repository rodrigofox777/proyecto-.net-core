﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProyectoIndimin.Aplicacion.DTO;
using ProyectoIndimin.Aplicacion.Interfaces;

namespace ProyectoIndimin.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TareaController : Controller
    {
        private readonly ITareaAplicacion _tareaAplicacion;

        public TareaController(ITareaAplicacion tareaAplicacion)
        {
            _tareaAplicacion = tareaAplicacion;
        }

        [HttpPost("Insert")]
        public async Task<IActionResult> Insert([FromBody] TareaDTO tareaDTO)
        {
            if (tareaDTO == null)
                return BadRequest();
            var response = await _tareaAplicacion.Insert(tareaDTO);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromBody] TareaDTO tareaDTO)
        {
            if (tareaDTO == null)
                return BadRequest();
            var response = await _tareaAplicacion.Update(tareaDTO);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpDelete("Delete/{tareaID}")]
        public async Task<IActionResult> Delete(int tareaID)
        {
            if (tareaID <= 0)
                return BadRequest();
            var response = await _tareaAplicacion.Delete(tareaID);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("Get/{tareaID}")]
        public async Task<IActionResult> Get(int tareaID)
        {
            if (tareaID <= 0)
                return BadRequest();
            var response = await _tareaAplicacion.Get(tareaID);
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("GetTareabyDay/{diaId}")]
        public async Task<IActionResult> GetTareabyDay(int diaId)
        {
            Response<IEnumerable<TareaDTO>> response;
            if (diaId < 0)
                return BadRequest();
            if (diaId == 0)
            {
                response = await _tareaAplicacion.GetAll();
            }
            else
            {
                response = await _tareaAplicacion.GetTareabyDay(diaId);
            }
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var response = await _tareaAplicacion.GetAll();
            if (response.IsSucess)
                return Ok(response);

            return BadRequest(response.Message);
        }
    }
}
