using Aplicacion;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProyectoIndimin.Dominio.Core;
using ProyectoIndimin.Dominio.Interfaces;
using Persistencia;
using ProyectoIndimin.Aplicacion.Interfaces;
using ProyectoIndimin.Aplicacion.Main;
using ProyectoIndimin.Dominio.Entidades;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("corsApp", builder => {
                builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            }));
            services.AddDbContext<Context>(opt => {
                opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                });
            services.AddControllers();
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddScoped<ICiudadano, CiudadanoDominio>();
            services.AddScoped<ICiudadanoAplicacion, CiudadanoAplicacion>();
            services.AddScoped<ITarea, TareaDominio>();
            services.AddScoped<ITareaAplicacion, TareaAplicacion>();
            services.AddScoped<IDiaSemana, DiaSemanaDominio>();
            services.AddScoped<IDiaSemanaAplicacion, DiaSemanaAplicacion>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Servicios para test Indimin",
                    Version = "v1"
                });
                c.CustomSchemaIds(c => c.FullName);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Context db)
        {
            app.UseCors("corsApp");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            db.Database.EnsureCreated();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI(
                c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Control Esclavos v1");
                });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
