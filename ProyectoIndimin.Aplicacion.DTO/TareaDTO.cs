﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoIndimin.Aplicacion.DTO
{
    public class TareaDTO
    {
        public int IdTarea { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int IdDia { get; set; }
        public string DiaSemana { get; set; }
        public int NroCiudadano { get; set; }
        public string Ciudadano { get; set; }
    }
}
