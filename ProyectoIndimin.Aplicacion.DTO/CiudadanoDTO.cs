﻿
namespace ProyectoIndimin.Aplicacion.DTO
{
    public class CiudadanoDTO
    {
        public int NroCiudadano { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
        public string Genero { get; set; }
    }
}
