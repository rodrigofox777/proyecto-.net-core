﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoIndimin.Aplicacion.DTO
{
    public class DiaSemanaDTO
    {
        public int IdDia { get; set; }
        public string Dia { get; set; }
    }
}
