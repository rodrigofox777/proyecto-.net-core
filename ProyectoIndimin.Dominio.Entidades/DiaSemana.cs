﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoIndimin.Dominio.Entidades
{
    public class DiaSemana
    {
        [Key]
        public int IdDia { get; set; }
        [Column(TypeName= "varchar(10)")]
        public string Dia { get; set; }
    }
}
