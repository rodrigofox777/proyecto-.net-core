﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoIndimin.Dominio.Entidades
{
    public class Tarea
    {
        [Key]
        public int IdTarea { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Nombre { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string Descripcion { get; set; }
        [ForeignKey("DiaSemana")]
        public int IdDia { get; set; }
        public DiaSemana DiaSemana { get; set; }
        [ForeignKey("Ciudadano")]
        public int NroCiudadano { get; set; }
        public Ciudadano Ciudadano { get; set; }
    }
}
