﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoIndimin.Dominio.Entidades
{
    public class Ciudadano
    {
        [Key]
        public int NroCiudadano { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Nombre { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Apellido { get; set; }
        [Column(TypeName = "int")]
        public int Edad { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string Genero { get; set; }
    }
}
