﻿using Microsoft.EntityFrameworkCore;
using ProyectoIndimin.Dominio.Entidades;

namespace Persistencia
{
    public class Context: DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DiaSemana>().HasKey(x => x.IdDia);
            modelBuilder.Entity<DiaSemana>().Property(x => x.IdDia).UseIdentityColumn();
            modelBuilder.Entity<DiaSemana>().Property(x => x.Dia).HasColumnType("varchar");
            modelBuilder.Entity<DiaSemana>().Property(x => x.Dia).HasMaxLength(10);

            modelBuilder.Entity<Ciudadano>().HasKey(x => x.NroCiudadano);
            modelBuilder.Entity<Ciudadano>().Property(x => x.NroCiudadano).UseIdentityColumn();
            modelBuilder.Entity<Ciudadano>().Property(x => x.Nombre).HasColumnType("varchar");
            modelBuilder.Entity<Ciudadano>().Property(x => x.Nombre).HasMaxLength(100);
            modelBuilder.Entity<Ciudadano>().Property(x => x.Apellido).HasColumnType("varchar");
            modelBuilder.Entity<Ciudadano>().Property(x => x.Apellido).HasMaxLength(100);
            modelBuilder.Entity<Ciudadano>().Property(x => x.Edad).HasColumnType("int");
            modelBuilder.Entity<Ciudadano>().Property(x => x.Genero).HasColumnType("varchar");
            modelBuilder.Entity<Ciudadano>().Property(x => x.Genero).HasMaxLength(10);

            modelBuilder.Entity<Tarea>().HasKey(x => x.IdTarea);
            modelBuilder.Entity<Tarea>().Property(x => x.IdTarea).UseIdentityColumn();
            modelBuilder.Entity<Tarea>().Property(x => x.Nombre).HasColumnType("varchar");
            modelBuilder.Entity<Tarea>().Property(x => x.Nombre).HasMaxLength(100);
            modelBuilder.Entity<Tarea>().Property(x => x.Descripcion).HasColumnType("varchar");
            modelBuilder.Entity<Tarea>().Property(x => x.Descripcion).HasMaxLength(200);

            modelBuilder.Entity<DiaSemana>().HasData(new DiaSemana[]
                    {
                        new DiaSemana() {IdDia=1, Dia ="Lunes"},
                        new DiaSemana() {IdDia=2,Dia = "Martes"},
                        new DiaSemana() {IdDia=3,Dia = "Miercoles"},
                        new DiaSemana() {IdDia=4,Dia = "Jueves"},
                        new DiaSemana() {IdDia=5,Dia = "Viernes"},
                        new DiaSemana() {IdDia=6,Dia = "Sabado"},
                        new DiaSemana() {IdDia=7,Dia = "Domingo"}
                    });
        }

        public DbSet<Ciudadano> Ciudadano { set; get; }
        public DbSet<DiaSemana> DiaSemana { set; get; }
        public DbSet<Tarea> Tarea { set; get; }
    }
}
