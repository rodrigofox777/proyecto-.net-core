﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Interfaces
{
    public interface IGetList<T>
    {
        Task<T> Get(int valueObjectId);
        Task<IEnumerable<T>> GetAll();
    }
}
