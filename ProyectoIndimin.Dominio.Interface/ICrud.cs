﻿using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Interfaces
{
    public interface ICrud<T>
    {
        Task<bool> Insert(T valueObject);
        Task<bool> Update(T valueObject);
        Task<bool> Delete(int valueObjectId);
    }
}
