﻿
using ProyectoIndimin.Dominio.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Interfaces
{
    public interface ICiudadano: IGetList<Ciudadano>,ICrud<Ciudadano>
    {
        Task<IEnumerable<Ciudadano>> GetNombre(string valueObjectId);
    }
}
