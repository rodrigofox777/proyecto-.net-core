﻿using ProyectoIndimin.Dominio.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Interfaces
{
    public interface ITarea : ICrud<Tarea>, IGetList<Tarea>
    {
        Task<IEnumerable<Tarea>> GetTareabyDay(int diaId);
    }
}
