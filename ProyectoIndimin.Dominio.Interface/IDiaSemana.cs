﻿using ProyectoIndimin.Dominio.Entidades;

namespace ProyectoIndimin.Dominio.Interfaces
{
    public interface IDiaSemana: IGetList<DiaSemana>
    {
    }
}
