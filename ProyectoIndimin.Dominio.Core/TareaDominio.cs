﻿using Microsoft.EntityFrameworkCore;
using Persistencia;
using ProyectoIndimin.Dominio.Entidades;
using ProyectoIndimin.Dominio.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Core
{
    public class TareaDominio : ITarea
    {
        private readonly Context _context;

        public TareaDominio(Context context)
        {
            _context = context;
        }
        public async Task<bool> Delete(int valueObjectId)
        {
            var tarea = await _context.Tarea.FindAsync(valueObjectId);
            if (tarea == null) return false;
            _context.Tarea.Remove(tarea);
            _context.SaveChanges();
            return true;
        }

        public async Task<Tarea> Get(int valueObjectId)
        {
            return await _context.Tarea.FindAsync(valueObjectId);
        }

        public async Task<IEnumerable<Tarea>> GetAll()
        {
            var tarea = await _context.Tarea.ToListAsync();
            return tarea;
        }
        public async Task<IEnumerable<Tarea>> GetTareabyDay(int valueObjectId)
        {
            var tarea = await _context.Tarea.AsQueryable().Where(x=> x.IdDia == valueObjectId).ToListAsync();
            return tarea;
        }

        public async Task<bool> Insert(Tarea valueObject)
        {
            _context.Tarea.Add(valueObject);
            var valor = await _context.SaveChangesAsync();
            return valor > 0 ? true : false;
        }

        public async Task<bool> Update(Tarea valueObjectId)
        {
            var tarea = await _context.Tarea.FindAsync(valueObjectId.IdTarea);
            if (tarea == null) return false;
            tarea.Nombre = valueObjectId.Nombre;
            tarea.IdDia = valueObjectId.IdDia;
            tarea.Descripcion = valueObjectId.Descripcion;
            tarea.NroCiudadano = valueObjectId.NroCiudadano;
            var valor = await _context.SaveChangesAsync();
            return valor > 0 ? true : false;
        }
    }
}
