﻿using Microsoft.EntityFrameworkCore;
using Persistencia;
using ProyectoIndimin.Dominio.Entidades;
using ProyectoIndimin.Dominio.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Core
{
    public class CiudadanoDominio : ICiudadano
    {
        private readonly Context _context;

        public CiudadanoDominio(Context context)
        {
            _context = context;
        }
        public async Task<bool> Delete(int valueObjectId)
        {
            var ciudadano = await _context.Ciudadano.FindAsync(valueObjectId);
            if (ciudadano == null) return false;
            _context.Ciudadano.Remove(ciudadano);
            _context.SaveChanges();
            return true;
        }

        public async Task<Ciudadano> Get(int valueObjectId)
        {
            return await _context.Ciudadano.FindAsync(valueObjectId);
        }

        public async Task<IEnumerable<Ciudadano>> GetNombre(string valueObjectId)
        {
            return await _context.Ciudadano.AsQueryable().Where(x => x.Nombre.Contains(valueObjectId) || x.Apellido.Contains(valueObjectId)).ToListAsync();
        }

        public async Task<IEnumerable<Ciudadano>> GetAll()
        {
            var ciudadanos = await _context.Ciudadano.ToListAsync();
            return ciudadanos;
        }

        public async Task<bool> Insert(Ciudadano valueObject)
        {
            _context.Ciudadano.Add(valueObject);
            var valor = await _context.SaveChangesAsync();
            return valor > 0 ? true : false;
        }

        public async Task<bool> Update(Ciudadano valueObjectId)
        {
            var ciudadano = await _context.Ciudadano.FindAsync(valueObjectId.NroCiudadano);
            if (ciudadano == null) return false;
            ciudadano.Nombre = valueObjectId.Nombre;
            ciudadano.Apellido = valueObjectId.Apellido;
            ciudadano.Genero = valueObjectId.Genero;
            ciudadano.Edad = valueObjectId.Edad;
            var valor = await _context.SaveChangesAsync();
            return valor > 0 ? true : false;
        }
    }
}
