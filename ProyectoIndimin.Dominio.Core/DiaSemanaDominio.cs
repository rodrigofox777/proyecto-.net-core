﻿using Microsoft.EntityFrameworkCore;
using Persistencia;
using ProyectoIndimin.Dominio.Entidades;
using ProyectoIndimin.Dominio.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProyectoIndimin.Dominio.Core
{
   public class DiaSemanaDominio: IDiaSemana
    {
        private readonly Context _context;

        public DiaSemanaDominio(Context context)
        {
            _context = context;
        }
        public async Task<DiaSemana> Get(int valueObjectId)
        {
            return await _context.DiaSemana.FindAsync(valueObjectId);
        }

        public async Task<IEnumerable<DiaSemana>> GetAll()
        {
            var tarea = await _context.DiaSemana.ToListAsync();
            return tarea;
        }
    }
}
