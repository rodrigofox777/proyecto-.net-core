﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoIndimin.Aplicacion.DTO
{
    public class Response<T>
    {
        public T Data { get; set; }
        public bool IsSucess { get; set; }
        public string Message { get; set; }

    }
}
