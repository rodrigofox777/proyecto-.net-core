﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ProyectoIndimin.Transversal.Comun
{
    public enum EnumMensajes
    {
        [EnumMember(Value = "Registro Exitoso")]
        INSERT,
        [EnumMember(Value = "Actualizacion Exitosa")]
        UPDATE,
        [EnumMember(Value = "Borrado Exitoso")]
        DELETE,
        [EnumMember(Value = "Consulta Exitosa")]
        QUERY,
    }
}
