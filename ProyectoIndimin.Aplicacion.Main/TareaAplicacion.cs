﻿using ProyectoIndimin.Dominio.Entidades;
using System;
using AutoMapper;
using ProyectoIndimin.Aplicacion.Interfaces;
using ProyectoIndimin.Dominio.Interfaces;
using ProyectoIndimin.Aplicacion.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProyectoIndimin.Transversal.Comun;
using System.Runtime.Serialization;

namespace ProyectoIndimin.Aplicacion.Main
{
    public class TareaAplicacion : ITareaAplicacion
    {
        private readonly ITarea _tareaDominio;
        private readonly IMapper _mapper;
        public TareaAplicacion(ITarea tareaDominio, IMapper mapper)
        {
            _tareaDominio = tareaDominio;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Insert(TareaDTO tareaDTO)
        {
            var response = new Response<bool>();
            try
            {
                var tarea = _mapper.Map<Tarea>(tareaDTO);
                response.Data = await _tareaDominio.Insert(tarea);
                if (response.Data)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.INSERT.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<bool>> Update(TareaDTO tareaDTO)
        {
            var response = new Response<bool>();
            try
            {
                var tarea = _mapper.Map<Tarea>(tareaDTO);
                response.Data = await _tareaDominio.Update(tarea);
                if (response.Data)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.UPDATE.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<bool>> Delete(int tareaID)
        {
            var response = new Response<bool>();
            try
            {
                response.Data = await _tareaDominio.Delete(tareaID);
                if (response.Data)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.DELETE.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<TareaDTO>> Get(int tareaID)
        {
            var response = new Response<TareaDTO>();
            try
            {
                var tarea = await _tareaDominio.Get(tareaID);
                response.Data = _mapper.Map<TareaDTO>(tarea);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<IEnumerable<TareaDTO>>> GetTareabyDay(int diaId)
        {
            var response = new Response<IEnumerable<TareaDTO>>();
            try
            {
                var tarea = await _tareaDominio.GetTareabyDay(diaId);
                response.Data = _mapper.Map<IEnumerable<TareaDTO>>(tarea);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<IEnumerable<TareaDTO>>> GetAll()
        {
            var response = new Response<IEnumerable<TareaDTO>>();
            try
            {
                var tarea = await _tareaDominio.GetAll();
                response.Data = _mapper.Map<IEnumerable<TareaDTO>>(tarea);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
