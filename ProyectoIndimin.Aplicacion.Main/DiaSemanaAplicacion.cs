﻿using AutoMapper;
using ProyectoIndimin.Aplicacion.DTO;
using ProyectoIndimin.Aplicacion.Interfaces;
using ProyectoIndimin.Dominio.Interfaces;
using ProyectoIndimin.Transversal.Comun;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace ProyectoIndimin.Aplicacion.Main
{
    public class DiaSemanaAplicacion: IDiaSemanaAplicacion
    {
        private readonly IDiaSemana _diaSemana;
        private readonly IMapper _mapper;
        public DiaSemanaAplicacion(IDiaSemana diaSemana, IMapper mapper)
        {
            _diaSemana = diaSemana;
            _mapper = mapper;
        }

        public async Task<Response<DiaSemanaDTO>> Get(int idDia)
        {
            var response = new Response<DiaSemanaDTO>();
            try
            {
                var diaSemana = await _diaSemana.Get(idDia);
                response.Data = _mapper.Map<DiaSemanaDTO>(diaSemana);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<IEnumerable<DiaSemanaDTO>>> GetAll()
        {
            var response = new Response<IEnumerable<DiaSemanaDTO>>();
            try
            {
                var diaSemana = await _diaSemana.GetAll();
                response.Data = _mapper.Map<IEnumerable<DiaSemanaDTO>>(diaSemana);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
