﻿using ProyectoIndimin.Dominio.Entidades;
using System;
using AutoMapper;
using ProyectoIndimin.Aplicacion.Interfaces;
using ProyectoIndimin.Dominio.Interfaces;
using ProyectoIndimin.Aplicacion.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProyectoIndimin.Transversal.Comun;
using System.Runtime.Serialization;

namespace ProyectoIndimin.Aplicacion.Main
{
    public class CiudadanoAplicacion: ICiudadanoAplicacion
    {
        private readonly ICiudadano _ciudadanoDominio;
        private readonly IMapper _mapper;
        public CiudadanoAplicacion(ICiudadano ciudadanoDominio, IMapper mapper)
        {
            _ciudadanoDominio = ciudadanoDominio;
            _mapper = mapper;
        }

        public async Task<Response<bool>> Insert(CiudadanoDTO customerDTO)
        {
            var response = new Response<bool>();
            try
            {
                var customer = _mapper.Map<Ciudadano>(customerDTO);
                response.Data = await _ciudadanoDominio.Insert(customer);
                if (response.Data)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.INSERT.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<bool>> Update(CiudadanoDTO ciudadanoDTO)
        {
            var response = new Response<bool>();
            try
            {
                var ciudadano = _mapper.Map<Ciudadano>(ciudadanoDTO);
                response.Data = await _ciudadanoDominio.Update(ciudadano);
                if (response.Data)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.UPDATE.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<bool>> Delete(int ciudadanoID)
        {
            var response = new Response<bool>();
            try
            {
                response.Data = await _ciudadanoDominio.Delete(ciudadanoID);
                if (response.Data)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.DELETE.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async Task<Response<CiudadanoDTO>> Get(int ciudadanoID)
        {
            var response = new Response<CiudadanoDTO>();
            try
            {
                var ciudadano = await _ciudadanoDominio.Get(ciudadanoID);
                response.Data = _mapper.Map<CiudadanoDTO>(ciudadano);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<Response<IEnumerable<CiudadanoDTO>>> GetNombre(string ciudadanoNombre)
        {
            var response = new Response<IEnumerable<CiudadanoDTO>>();
            try
            {
                var ciudadano = await _ciudadanoDominio.GetNombre(ciudadanoNombre);
                response.Data = _mapper.Map<IEnumerable<CiudadanoDTO>>(ciudadano);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
        public async  Task<Response<IEnumerable<CiudadanoDTO>>> GetAll()
        {
            var response = new Response<IEnumerable<CiudadanoDTO>>();
            try
            {
                var customer = await _ciudadanoDominio.GetAll();
                response.Data = _mapper.Map<IEnumerable<CiudadanoDTO>>(customer);
                if (response.Data != null)
                {
                    response.IsSucess = true;
                    response.Message = EnumMensajes.QUERY.GetAttributeOfType<EnumMemberAttribute>().Value;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
