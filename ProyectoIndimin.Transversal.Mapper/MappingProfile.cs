﻿using AutoMapper;
using ProyectoIndimin.Aplicacion.DTO;
using ProyectoIndimin.Dominio.Entidades;

namespace Aplicacion
{
    public class MappingProfile: Profile
    {
        public MappingProfile() {
            CreateMap<Ciudadano, CiudadanoDTO>().ReverseMap();
            CreateMap<DiaSemana, DiaSemanaDTO>().ReverseMap();
            CreateMap<Tarea, TareaDTO>().ReverseMap();
            //.ForMember(x => x.Marca, y => y.MapFrom(z => z.Marca.Nombre));
        }
    }
}
